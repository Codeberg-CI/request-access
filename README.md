# Getting access to the Codeberg CI

**This is outdated. Please check https://codeberg.org/Codeberg-e.V./requests to learn about how to apply for CI access.**

---

Hey there, and thank you for your interest in the Codeberg CI.
Due to the high demand, we want to bring a hosted instance to you and your FLOSS projects.

Please understand that we are currently in a **closed testing phase**.
However, it is possible to apply for early access, so please read on.

We are using [Woodpecker CI](https://woodpecker-ci.org) as our CI solution.
It is an early and libre fork of Drone, before licence changes were made.
We think it fits ideal for Codeberg, as it is a simple solution that satisfies the majority of project needs,
and it can be easily selfhosted by everyone to avoid vendor lock-ins to Codeberg in case we ran a super-complicated setup.


## Disclaimers and caveats

Although we and the awesome maintainers behind Woodpecker CI do a lot to create an awesome user experience,
the rocket start in development activity only occured very recently.
Since we are always running latest edge code, you might notice bugs, especially in the UI/UX, or lack features of proprietary competitors.

Also, we at Codeberg do our best to keep the hosted service stable and accessible,
but we do not make any guarantees of any kind.

**CI access is provided as-is and might break at any time and for an undefined period of time**,
due to server issues, for testing and maintenance purpose or human error.


## Application

For gaining access to Woodpecker CI, you have to apply by [creating an issue in this very repository](https://codeberg.org/Codeberg-CI/request-access/issues/new).
Access is currently granted per-user, not per repository, organization or project.
Keep this in mind when working with multiple collabotators, you can also name multiple users in one request.

Please tell us for which projects you'd like to test CI access for. Also note, that we might reject requests from new Codeberg users, as we want to prevent abuse of our computing power via spam accounts. If you just joined Codeberg for testing CI access, please give us a link where we can find your previous projects / contributions.


## Rules of Usage

For the usage of the Codeberg Hosted CI, the following rules apply:

- All code must be carrying a valid Free/Libre Open Source Software license as defined by the FSF/OSI (note this is a general requirement for code hosting at Codeberg).
- The repository needs to be publicly accessible. In case you need pre-release / staging areas, please explicitly request this in the issue tracker.
- The project's purpose must be described briefly in the README or repository description, and match the implementation.
- Resource usage must be reasonable for the intended use-case. This is determined on a case-by-case basis, but please be aware that CI uses a lot of computing resources (cloning your repo and container, installing all your required tools, building and throwing every thing away) which costs us money and does damage to our precious environment. Therefore, please consider twice how to create a good balance between ensuring code quality for your project and resource usage therefore.
- As an early tester, you are responsible for improving the product within your means. Please read the contributing section below.


## Getting Started

In order to get started, please check out the [upstream intro](https://woodpecker-ci.org/docs/intro).
Then visit [ci.codeberg.org](https://ci.codeberg.org), log in by granting the CI access to your Codeberg account,
then follow the UI steps to enable CI for your repositories.


## Contributing

Contributions to the Codeberg CI and Woodpecker are warmly welcome. We will outline a few options below:

- joining the [Matrix Channel](https://matrix.to/#/%23codeberg-ci:obermui.de) for feedback, discussion, exchange and helping others, as well as [giving feedback](https://codeberg.org/Codeberg-CI/feedback/issues) as soon as you got access to the CI
- reporting any bugs as you find to the places above, to the [upstream Woodpecker repository](https://github.com/woodpecker-ci/woodpecker) or to our [local mirror](https://codeberg.org/Codeberg-CI/woodpecker)
- help in fixing bugs at the places just mentioned, as well as taking over maintenance for some (long?) time by reviewing pull requests etc
- giving tips and tricks as well as working on proper documentation for Woodpecker and the hosted service (feel free to contact us to get started here, work has not been bootstrapped yet)
- contacting us to join the infrastructure efforts, allowing to set up the infrastructure and improve reliability
- [donating to Codeberg](https://docs.codeberg.org/improving-codeberg/#donate-to-codeberg) which allows scaling the infrastructure and fund the further development

